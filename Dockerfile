# syntax=docker/dockerfile:1
FROM node:lts-bullseye-slim
SHELL ["/bin/bash", "--login", "-c"]

# Install git
RUN apt-get update && apt-get install -y git

# Install pnpm
RUN corepack enable
RUN corepack prepare pnpm@latest --activate
RUN pnpm config set store-dir .pnpm-store
